import java.util.Arrays;

public class DynamicDoubleArray {

    private Double[] arr = new Double[10];

    // Получаем элемент по индексу
    public double get(int index) {
        trim(arr);
        if (index < arr.length) {
            return arr[index];
        } else {
            throw new ArrayIndexOutOfBoundsException();
        }
    }

    // удаляем последний элемент
    public void delete() {
        Double[] temp = new Double[arr.length - 1];
        for (int i = 0; i < temp.length; i++) {
            temp[i] = arr[i];
        }
        arr = temp;
    }

    // удаление по индексу
    public void delete(int index) {
        Double[] temp = new Double[arr.length - 1];
        int x = 0;
        for (int i = 0; i < arr.length; i++) {
            if (i == index) {
                continue;
            }
            temp[x] = arr[i];
            x++;
        }
        arr = temp;
    }

    // добавление элемента.
    public void add(Double num) {
        if (arr[arr.length - 1] != null) {
            arr = Arrays.copyOf(arr, 2 * arr.length);
            for (int i = 0; i < arr.length * 2; i++) {
                if (arr[i] == null) {
                    arr[i] = num;
                    break;
                }
            }
        } else {
            for (int i = 0; i < arr.length; i++) {
                if (arr[i] == null) {
                    arr[i] = num;
                    break;
                }
            }
        }
    }

    // замена значения по индексу
    public void set(int index, double num) {
        arr[index] = num;
    }

    // Удаляем пустые элементы массива
    private void trim(Double[] arr) {
        Double[] temp = new Double[size()];
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == null) {
                break;
            }
            temp[i] = arr[i];
        }
        this.arr = temp;
    }


    public int size() {
        int sizeArr = 0;
        for (Double anArr : arr) {
            if (anArr != null) {
                sizeArr++;
            }
        }
        return sizeArr;
    }

}
